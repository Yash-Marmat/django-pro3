
from django.shortcuts import render
from .models import Post
from django.urls import reverse_lazy

from django.views.generic.edit import (
    CreateView,   # helps the user to create or add a new post
    UpdateView,
    DeleteView,
)

from django.views.generic import (
    ListView,
    DetailView,
)


class BlogView(ListView):
    model         = Post
    template_name = 'home.html'

class BlogsDetail(DetailView):
    model         = Post
    template_name = 'post_detail.html'

class BlogCreate(CreateView):
    model = Post
    template_name = 'post_new.html'
    fields = '__all__'

'''
Within BlogCreateView we specify our database model Post, the name of our template
post_new.html, and all fields with '__all__' since we only have two: title and author
'''

class BlogUpdate(UpdateView):
    model         = Post
    template_name = 'post_edit.html'
    fields        = ['title', 'body']

class BlogDelete(DeleteView):
    model         = Post
    template_name = 'post_delete.html'
    success_url   = reverse_lazy('home')