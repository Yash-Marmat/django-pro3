from django.db import models
from django.urls import reverse  # it will redirect us after saving our new blog post


class Post(models.Model):
    title  = models.CharField(max_length = 200)
    body   = models.TextField()
    author = models.ForeignKey(
        'auth.user',
        on_delete = models.CASCADE,
    ) 

    def __str__(self):
        return self.title

    def get_absolute_url(self):   # it will redirect us to our newly saved blog post (will show its title and description)
        return reverse('post_detail', args = [str(self.id)])